package com.example.guidefi.Common.Helpers;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerFragmentManager extends FragmentPagerAdapter {

    public List <Fragment> fragmentList;

    public ViewPagerFragmentManager(@NonNull androidx.fragment.app.FragmentManager fm) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragmentList = new ArrayList<>();
    }

    public void addFragments(Fragment fragment){
        this.fragmentList.add(fragment);

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return this.fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
