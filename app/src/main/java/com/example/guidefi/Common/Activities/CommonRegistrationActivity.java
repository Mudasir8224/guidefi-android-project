package com.example.guidefi.Common.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.Adviser.Activities.AdviserApplyActivity;
import com.example.guidefi.Client.Activities.ClientProfileSignUpActivity;
import com.example.guidefi.R;

public class CommonRegistrationActivity extends AppCompatActivity {
    Button buttonClientSite;
    Button buttonAdviserSite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_common);

        init();
        clickEvents();



    }

    private void init() {
        buttonClientSite = findViewById(R.id.buttonClientSite);
        buttonAdviserSite = findViewById(R.id.buttonAdviserSite);
    }

    private  void clickEvents(){

        buttonClientSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CommonRegistrationActivity.this, ClientProfileSignUpActivity.class);
                startActivity(intent);
            }
        });

        buttonAdviserSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CommonRegistrationActivity.this, AdviserApplyActivity.class);
                startActivity(intent);
            }
        });

    }
}
