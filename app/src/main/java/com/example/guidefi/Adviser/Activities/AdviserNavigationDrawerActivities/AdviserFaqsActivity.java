package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Adviser.Adapters.AdviserFaqsActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserPaymentsActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserFaqsModel;
import com.example.guidefi.Adviser.Models.AdviserPaymentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserFaqsActivity extends AppCompatActivity {

    RecyclerView adviserFaqsRv;
    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_faqs);


        init();
        setRecyclerView();
        clickEvents();

    }


    private void init(){
        adviserFaqsRv = findViewById(R.id.adviserFaqsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserFaqsRv.setLayoutManager(layoutManager);
        List<AdviserFaqsModel> faqsModels = new ArrayList<>();

        faqsModels.add(new AdviserFaqsModel("Question 1","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new AdviserFaqsModel("Question 2","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new AdviserFaqsModel("Question 3","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new AdviserFaqsModel("Question 4","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));

        AdviserFaqsActivityAdapter adapter = new AdviserFaqsActivityAdapter(faqsModels,this);
        adviserFaqsRv.setAdapter(adapter);


    }

    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }



}