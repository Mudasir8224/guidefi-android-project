package com.example.guidefi.Adviser.Fragments.SubModuleFragmentsAdviserClients;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardAllClientsSubFragmentAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.Client.Adapters.ClientMyAdvisersFragmentAdapter;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardAllClientsSubFragment extends Fragment {

   RecyclerView adviserAllClientsSubFragmentRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_adviser_dashboard_all_clients_sub, container, false);
        adviserAllClientsSubFragmentRv = rootView.findViewById(R.id.adviserAllClientsSubFragmentRv);
         setRecyclerView();
        return rootView;

    }

        private void setRecyclerView (){
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            adviserAllClientsSubFragmentRv.setLayoutManager(layoutManager);
            List<AdviserHomeModel> homeList = new ArrayList<>();
            homeList.add(new AdviserHomeModel());
            homeList.add(new AdviserHomeModel());
            homeList.add(new AdviserHomeModel());
            homeList.add(new AdviserHomeModel());
            homeList.add(new AdviserHomeModel());
            AdviserDashboardAllClientsSubFragmentAdapter adapter = new AdviserDashboardAllClientsSubFragmentAdapter(homeList,getContext());
            adviserAllClientsSubFragmentRv.setAdapter(adapter);

        }
    }
