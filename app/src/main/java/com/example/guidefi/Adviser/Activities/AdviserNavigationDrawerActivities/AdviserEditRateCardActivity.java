package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.guidefi.R;

public class AdviserEditRateCardActivity extends AppCompatActivity {

    ImageView imageViewLeftArrow;
    LinearLayout layoutSaveRateCardDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_edit_rate_card);


        init();
        clickEvents();

    }


    private void init(){

        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);
        layoutSaveRateCardDetail = findViewById(R.id.layoutSaveRateCardDetail);


    }


    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


        layoutSaveRateCardDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

    }

}