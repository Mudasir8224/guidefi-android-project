package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class AdviserRegistrationMessageActivity extends AppCompatActivity {

    Button buttonRegistrationOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_registration_message);

        buttonRegistrationOk = findViewById(R.id.buttonRegistrationOk);

        buttonRegistrationOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserRegistrationMessageActivity.this,AdviserProfileApproveMessageActivity.class);
                startActivity(intent);
            }
        });
    }
}