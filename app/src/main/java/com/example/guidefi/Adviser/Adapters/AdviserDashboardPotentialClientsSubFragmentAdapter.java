package com.example.guidefi.Adviser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Activities.AdviserAcceptClientRequestActivity;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.List;

public class AdviserDashboardPotentialClientsSubFragmentAdapter extends RecyclerView.Adapter<AdviserDashboardPotentialClientsSubFragmentAdapter.viewHolderMostPopular> {

    List<AdviserHomeModel> adviserHomeModelList;
    private Context context;

    public AdviserDashboardPotentialClientsSubFragmentAdapter(List<AdviserHomeModel> adviserHomeModelList, Context context) {
        this.adviserHomeModelList = adviserHomeModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.adviser_dashboard_potentialclients_subfragment_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final AdviserHomeModel adviserHomeModel = adviserHomeModelList.get(position);


    }

    @Override
    public int getItemCount() {
        return adviserHomeModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        Button button;

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

            button = itemView.findViewById(R.id.buttonAdviserAcceptPotentialClient);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, AdviserAcceptClientRequestActivity.class);
                    context.startActivity(intent);
                }
            });

        }
    }
}
