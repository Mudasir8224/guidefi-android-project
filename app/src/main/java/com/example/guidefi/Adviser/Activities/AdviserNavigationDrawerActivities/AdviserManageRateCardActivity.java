package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.guidefi.Adviser.Adapters.AdviserManageRateCardActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserManageRateCardActivity extends AppCompatActivity {

    RecyclerView adviserManageRateCardRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_manage_rate_card);

        init();
        setRecyclerView();


    }


    private void init(){
        adviserManageRateCardRv = findViewById(R.id.adviserManageRateCardRv);


    }



    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserManageRateCardRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserManageRateCardActivityAdapter adapter = new AdviserManageRateCardActivityAdapter(homeList,this);
        adviserManageRateCardRv.setAdapter(adapter);

    }








}