package com.example.guidefi.Adviser.Activities.AdviserDashboardActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserDebitAndCreditCardsActivity;
import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserFaqsActivity;
import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserManageRateCardActivity;
import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserNavTermsAndConditionActivity;
import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserPaymentsActivity;
import com.example.guidefi.Adviser.Fragments.AdviserDashboardAdviserProfileFragment;
import com.example.guidefi.Adviser.Fragments.AdviserDashboardAllClientsFragment;
import com.example.guidefi.Adviser.Fragments.AdviserDashboardHomeFragment;
import com.example.guidefi.Common.Helpers.ViewPagerFragmentManager;
import com.example.guidefi.Common.Views.ViewPagerV2;
import com.example.guidefi.R;
import com.google.android.material.navigation.NavigationView;

public class AdviserDashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ViewPagerV2 viewpagerAdviserDashboard;
    ViewPagerFragmentManager viewPagerFragmentManager;
    TextView textViewAdviserHomeFragment;
    TextView textViewAdviserClientFragment;
    TextView textViewAdviserAnalyticsFragment;
    TextView textViewAdviserProfileFragment;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_dashboard);

        init();
        setupFragment();
        clickEvent();
        businessLogic();
        setupDrawer();


    }

    private void init(){
        viewpagerAdviserDashboard = findViewById(R.id.viewpagerAdviserDashboard);
        textViewAdviserHomeFragment = findViewById(R.id.textViewAdviserHomeFragment);
        textViewAdviserClientFragment = findViewById(R.id.textViewAdviserClientFragment);
        textViewAdviserAnalyticsFragment = findViewById(R.id.textViewAdviserAnalyticsFragment);
        textViewAdviserProfileFragment = findViewById(R.id.textViewAdviserProfileFragment);

        drawerLayout = findViewById(R.id.mDrawer);
        navigationView = findViewById(R.id.nav_view);

        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);


    }



    private void setupFragment(){
        this.viewPagerFragmentManager = new ViewPagerFragmentManager(getSupportFragmentManager());
        this.viewPagerFragmentManager.addFragments(new AdviserDashboardHomeFragment());
        this.viewPagerFragmentManager.addFragments(new AdviserDashboardAllClientsFragment());
        this.viewPagerFragmentManager.addFragments(new AdviserDashboardAdviserProfileFragment());
        this.viewPagerFragmentManager.addFragments(new Fragment());
        viewpagerAdviserDashboard.setAdapter(viewPagerFragmentManager);
        viewpagerAdviserDashboard.disablescroll(true);

    }

    private void clickEvent(){

        textViewAdviserHomeFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewpagerAdviserDashboard.setCurrentItem(0,true);
                textViewAdviserHomeFragment.setTextColor(Color.WHITE);
                textViewAdviserAnalyticsFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserProfileFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserClientFragment.setTextColor(Color.parseColor("#DFDADC"));

            }
        });


        textViewAdviserClientFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewpagerAdviserDashboard.setCurrentItem(1,true);
                textViewAdviserClientFragment.setTextColor(Color.WHITE);
                textViewAdviserAnalyticsFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserProfileFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserHomeFragment.setTextColor(Color.parseColor("#DFDADC"));
            }
        });


        textViewAdviserProfileFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewpagerAdviserDashboard.setCurrentItem(2,true);
                textViewAdviserProfileFragment.setTextColor(Color.WHITE);
                textViewAdviserAnalyticsFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserClientFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserHomeFragment.setTextColor(Color.parseColor("#DFDADC"));
            }
        });


        textViewAdviserAnalyticsFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewpagerAdviserDashboard.setCurrentItem(3,true);
                textViewAdviserAnalyticsFragment.setTextColor(Color.WHITE);
                textViewAdviserProfileFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserClientFragment.setTextColor(Color.parseColor("#DFDADC"));
                textViewAdviserHomeFragment.setTextColor(Color.parseColor("#DFDADC"));

            }
        });


    }


    private void businessLogic(){
        textViewAdviserHomeFragment.setTextColor(Color.WHITE);
        textViewAdviserClientFragment.setTextColor(Color.parseColor("#DFDADC"));
        textViewAdviserAnalyticsFragment.setTextColor(Color.parseColor("#DFDADC"));
        textViewAdviserProfileFragment.setTextColor(Color.parseColor("#DFDADC"));


    }


    public void setupDrawer () {

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,
                toolbar,
                R.string.open,
                R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.adviserLeftNavigationPayments:
                Intent intentPayment = new Intent(AdviserDashboardActivity.this, AdviserPaymentsActivity.class);
                startActivity(intentPayment);
                break;


            case R.id.adviserLeftNavigationDebitAndCreditCards:
                Intent intentDebitAndCreditCards = new Intent(AdviserDashboardActivity.this, AdviserDebitAndCreditCardsActivity.class);
                startActivity(intentDebitAndCreditCards);
                break;


            case R.id.adviserLeftNavigationRateCards:
                Intent intentRateCards = new Intent(AdviserDashboardActivity.this, AdviserManageRateCardActivity.class);
                startActivity(intentRateCards);
                break;


            case R.id.adviserLeftNavigationTermsAnCondition:
                Intent intentTermsAndCondition = new Intent(AdviserDashboardActivity.this, AdviserNavTermsAndConditionActivity.class);
                startActivity(intentTermsAndCondition);
                break;


            case R.id.adviserLeftNavigationFaqs:
                Intent intentFaqs = new Intent(AdviserDashboardActivity.this, AdviserFaqsActivity.class);
                startActivity(intentFaqs);
                break;


        }



        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}