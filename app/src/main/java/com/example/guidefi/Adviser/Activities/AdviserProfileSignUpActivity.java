package com.example.guidefi.Adviser.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guidefi.Adviser.Activities.AdviserAddRateCardActivities.AdviserRateCardActivity;
import com.example.guidefi.R;

public class AdviserProfileSignUpActivity extends AppCompatActivity {

    Button buttonSignUpNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_profile_sign_up);

        buttonSignUpNext = findViewById(R.id.buttonSignUpNext);
        buttonSignUpNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserProfileSignUpActivity.this, AdviserRateCardActivity.class);
                startActivity(intent);
            }
        });

    }
}