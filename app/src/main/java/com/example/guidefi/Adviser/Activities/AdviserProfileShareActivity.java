package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.R;

public class AdviserProfileShareActivity extends AppCompatActivity {

    ImageView imageViewProfileShareBackArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_profile_share);
        init();
        clickEvents();


    }

    private void init(){
        imageViewProfileShareBackArrow= findViewById(R.id.imageViewProfileShareBackArrow);
    }

    private void clickEvents(){
        imageViewProfileShareBackArrow .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}