package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeAllVideosActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeSeeAllQuotesActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardSeeAllQuotesActivity extends AppCompatActivity {

    RecyclerView adviserDashboardHomeAllQuotesActivityRv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_dashboard_see_all_quotes);
        init();
        setRecyclerViewAllSeeQuotes();


    }

    public void init(){
        adviserDashboardHomeAllQuotesActivityRv = findViewById(R.id.adviserDashboardHomeAllQuotesActivityRv);

    }

    private void setRecyclerViewAllSeeQuotes (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDashboardHomeAllQuotesActivityRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardHomeSeeAllQuotesActivityAdapter adapter = new AdviserDashboardHomeSeeAllQuotesActivityAdapter(homeList,this);
        adviserDashboardHomeAllQuotesActivityRv.setAdapter(adapter);

    }

    private void clickEvents(){

    }


}