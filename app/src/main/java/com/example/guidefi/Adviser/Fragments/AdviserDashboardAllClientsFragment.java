package com.example.guidefi.Adviser.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.guidefi.Adviser.Fragments.SubModuleFragmentsAdviserClients.AdviserDashboardAllClientsSubFragment;
import com.example.guidefi.Adviser.Fragments.SubModuleFragmentsAdviserClients.AdviserDashboardPotentialClientsSubFragment;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientAdviserSubFragment;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientConnectedAdviserSubFragment;
import com.example.guidefi.Common.Helpers.ViewPagerFragmentManager;
import com.example.guidefi.Common.Views.ViewPagerV2;
import com.example.guidefi.R;

public class AdviserDashboardAllClientsFragment extends Fragment {

    Button buttonAdviserALLClientsFragment;
    Button buttonAdviserPotentialClientsFragment;
    ViewPagerV2 viewPagerAdviserDashboardClients;
    ViewPagerFragmentManager viewPagerFragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_adviser_dashboard_all_clients, container, false);

        buttonAdviserALLClientsFragment = rootView.findViewById(R.id.buttonAdviserALLClientsFragment);
        buttonAdviserPotentialClientsFragment = rootView.findViewById(R.id.buttonAdviserPotentialClientsFragment);
        viewPagerAdviserDashboardClients = rootView.findViewById(R.id.viewPagerAdviserDashboardClients);

        setupFragment();
        clickEventAllClientsFragment();
        clickEventPotentialClientsFragment();
        businessLogic();
        return rootView;
    }

    private void setupFragment() {
        this.viewPagerFragmentManager = new ViewPagerFragmentManager(getChildFragmentManager());
        this.viewPagerFragmentManager.addFragments(new AdviserDashboardAllClientsSubFragment());
        this.viewPagerFragmentManager.addFragments(new AdviserDashboardPotentialClientsSubFragment());
        this.viewPagerFragmentManager.addFragments(new Fragment());
        viewPagerAdviserDashboardClients.setAdapter(viewPagerFragmentManager);
        viewPagerAdviserDashboardClients.disablescroll(true);

    }


    private void clickEventAllClientsFragment() {

        buttonAdviserALLClientsFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPagerAdviserDashboardClients.setCurrentItem(0, true);
                buttonAdviserPotentialClientsFragment.setTextColor(Color.parseColor("#736469"));
                buttonAdviserALLClientsFragment.setTextColor(Color.BLACK);
            }
        });


    }

    private void clickEventPotentialClientsFragment() {

        buttonAdviserPotentialClientsFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPagerAdviserDashboardClients.setCurrentItem(1, true);

                buttonAdviserALLClientsFragment.setTextColor(Color.parseColor("#736469"));
                buttonAdviserPotentialClientsFragment.setTextColor(Color.BLACK);

            }

        });


    }

    private void businessLogic() {

        buttonAdviserALLClientsFragment.setTextColor(Color.BLACK);
        buttonAdviserPotentialClientsFragment.setTextColor(Color.parseColor("#736469"));


    }
}