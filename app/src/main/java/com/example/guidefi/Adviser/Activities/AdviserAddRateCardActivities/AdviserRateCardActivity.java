package com.example.guidefi.Adviser.Activities.AdviserAddRateCardActivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guidefi.Adviser.Activities.AdviserTermAndConditionActivity;
import com.example.guidefi.R;

public class AdviserRateCardActivity extends AppCompatActivity {

        Button buttonRateCardNext;
        Button buttonAdviserAddRateCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_rate_card);

        init();
        clickEvents();

    }

    private void init(){

        buttonRateCardNext = findViewById(R.id.buttonRateCardNext);
        buttonAdviserAddRateCard = findViewById(R.id.buttonAdviserAddRateCard);


    }

    private void clickEvents(){

        buttonRateCardNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserRateCardActivity.this, AdviserTermAndConditionActivity.class);
                startActivity(intent);

            }
        });


        buttonAdviserAddRateCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserRateCardActivity.this, AdviserRateCardFieldsActivity.class);
                startActivity(intent);

            }
        });

    }

}