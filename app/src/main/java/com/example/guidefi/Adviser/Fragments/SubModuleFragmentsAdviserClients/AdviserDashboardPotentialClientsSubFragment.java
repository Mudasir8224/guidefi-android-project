package com.example.guidefi.Adviser.Fragments.SubModuleFragmentsAdviserClients;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardAllClientsSubFragmentAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardPotentialClientsSubFragmentAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardPotentialClientsSubFragment extends Fragment {

    RecyclerView adviserPotentialClientsSubFragmentRv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_adviser_dashboard_potential_clients_sub, container, false);

        adviserPotentialClientsSubFragmentRv = rootView.findViewById(R.id.adviserPotentialClientsSubFragmentRv);
        setRecyclerView();

        return rootView;
    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserPotentialClientsSubFragmentRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardPotentialClientsSubFragmentAdapter adapter = new AdviserDashboardPotentialClientsSubFragmentAdapter(homeList,getContext());
        adviserPotentialClientsSubFragmentRv.setAdapter(adapter);

    }


}