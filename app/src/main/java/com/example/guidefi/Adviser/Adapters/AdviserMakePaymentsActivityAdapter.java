package com.example.guidefi.Adviser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserMakePaymentsActivity;
import com.example.guidefi.Adviser.Models.AdviserPaymentsModel;
import com.example.guidefi.R;

import java.util.List;

import static com.example.guidefi.Client.Models.ClientHomeModel.FIRST_LAYOUT;
import static com.example.guidefi.Client.Models.ClientHomeModel.SECOND_LAYOUT;

public class AdviserMakePaymentsActivityAdapter extends RecyclerView.Adapter {

    private List<AdviserPaymentsModel> paymentsModelList;
    private Context context;

    public AdviserMakePaymentsActivityAdapter(List<AdviserPaymentsModel> paymentsModelList, Context context) {
        this.paymentsModelList = paymentsModelList;
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {
        //  return super.getItemViewType(position);
        switch (paymentsModelList.get(position).getViewType()) {

            case 0:
                return FIRST_LAYOUT;
            case 1:
                return SECOND_LAYOUT;
            default:
                return -1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case FIRST_LAYOUT:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.adviser_makepayments_services_view, parent, false);
                return new firstLayout(view);
            case SECOND_LAYOUT:
               LayoutInflater inflater2 = LayoutInflater.from(parent.getContext());
                View view2 = inflater2.inflate(R.layout.adviser_makepayments_payment_view, parent, false);
                return new secondLayout(view2);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


    }


    @Override
    public int getItemCount() {
        return this.paymentsModelList.size();
    }

    class firstLayout extends RecyclerView.ViewHolder {


        public firstLayout(@NonNull View itemView) {
            super(itemView);

        }
    }

    class secondLayout extends RecyclerView.ViewHolder {

        public secondLayout(@NonNull View itemView) {
            super(itemView);
        }
    }




}
