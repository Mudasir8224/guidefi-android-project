package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Adviser.Adapters.AdviserPaymentsActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserPaymentsModel;
import com.example.guidefi.Client.Adapters.ClientDashboardHomeAdapter;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserPaymentsActivity extends AppCompatActivity {

    RecyclerView adviserPaymentsRv;
    ImageView imageViewLeftArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_payments);

        init();
        setRecyclerView();
        clickEvents();


    }



    private void init(){
        adviserPaymentsRv = findViewById(R.id.adviserPaymentsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserPaymentsRv.setLayoutManager(layoutManager);
        List<AdviserPaymentsModel> homeList = new ArrayList<>();
        homeList.add(new AdviserPaymentsModel(AdviserPaymentsModel.FIRST_LAYOUT));
        homeList.add(new AdviserPaymentsModel(AdviserPaymentsModel.SECOND_LAYOUT));
        homeList.add(new AdviserPaymentsModel(AdviserPaymentsModel.FIRST_LAYOUT));
        homeList.add(new AdviserPaymentsModel(AdviserPaymentsModel.SECOND_LAYOUT));
        homeList.add(new AdviserPaymentsModel(AdviserPaymentsModel.SECOND_LAYOUT));
        AdviserPaymentsActivityAdapter adapter = new AdviserPaymentsActivityAdapter(homeList,this);
        adviserPaymentsRv.setAdapter(adapter);

    }




    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


}