package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Adviser.Adapters.AdviserDebitAndCreditCardsActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDebitAndCreditCardsActivity extends AppCompatActivity {

    RecyclerView adviserDebitAndCreditCardsRv;
    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_debit_and_credit_cards);

        init();
        setRecyclerView();
        clickEvents();


    }



    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDebitAndCreditCardsRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDebitAndCreditCardsActivityAdapter adapter = new AdviserDebitAndCreditCardsActivityAdapter(homeList,this);
        adviserDebitAndCreditCardsRv.setAdapter(adapter);

    }


    private void init(){
        adviserDebitAndCreditCardsRv = findViewById(R.id.adviserDebitAndCreditCardsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);


    }

    private void clickEvents(){
        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



}