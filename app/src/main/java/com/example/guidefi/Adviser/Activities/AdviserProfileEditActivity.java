package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.guidefi.R;

public class AdviserProfileEditActivity extends AppCompatActivity {

    Button buttonAdviserEditProfileCancel;
    Button buttonAdviserEditProfileOk;
    ImageView imageViewAdviserEditProfileArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_profile_edit);

        init();
        clickEvents();


    }

    private void init(){
        imageViewAdviserEditProfileArrow = findViewById(R.id.imageViewAdviserEditProfileArrow);
        buttonAdviserEditProfileCancel = findViewById(R.id.buttonAdviserEditProfileCancel);
        buttonAdviserEditProfileOk = findViewById(R.id.buttonAdviserEditProfileOk);
    }

    private void clickEvents(){

        buttonAdviserEditProfileCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonAdviserEditProfileOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        imageViewAdviserEditProfileArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}