package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.guidefi.R;

public class AdviserLogInActivity extends AppCompatActivity {

    TextView textViewResetPassword;
    Button buttonAdviserLogIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_log_in);

        init();
        clickEvents();

    }

    private void init(){

        textViewResetPassword = findViewById(R.id.textViewResetPassword);
        buttonAdviserLogIn = findViewById(R.id.buttonAdviserLogIn);
    }

    private void clickEvents(){
        textViewResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserLogInActivity.this,AdviserResetPasswordActivity.class);
                startActivity(intent);
            }
        });

        buttonAdviserLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserLogInActivity.this,AdviserGuideMatchActivity.class);
                startActivity(intent);
            }
        });
    }
}