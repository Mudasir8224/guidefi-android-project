package com.example.guidefi.Adviser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.guidefi.Adviser.Activities.AdviserDashboardActivity.AdviserDashboardActivity;
import com.example.guidefi.Adviser.Activities.AdviserDashboardAllVideosActivity;
import com.example.guidefi.Adviser.Activities.AdviserDashboardSeeAllQuotesActivity;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeAllVideosActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeQuotesFragmentAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeVideosFragmentAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardHomeFragment extends Fragment {
          RecyclerView adviserDashboardHomeVideosRv;
          RecyclerView adviserDashboardHomeQuotesRv;
          TextView textViewAdviserHomeFragmentAllVideos;
          TextView textViewAdviserHomeFragmentAllQuotes;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_adviser_dashboard_home, container, false);
        adviserDashboardHomeVideosRv =rootView.findViewById(R.id.adviserDashboardHomeVideosRv);
        adviserDashboardHomeQuotesRv = rootView.findViewById(R.id.adviserDashboardHomeQuotesRv);
        textViewAdviserHomeFragmentAllVideos = rootView.findViewById(R.id.textViewAdviserHomeFragmentAllVideos);
        textViewAdviserHomeFragmentAllQuotes = rootView.findViewById(R.id.textViewAdviserHomeFragmentAllQuotes);

        setRecyclerViewVideo();
        setRecyclerViewQuotes();
        clickEvent();
        return rootView;
    }


    private void setRecyclerViewVideo (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDashboardHomeVideosRv.setLayoutManager(layoutManager);
        List <AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardHomeVideosFragmentAdapter adapter = new AdviserDashboardHomeVideosFragmentAdapter(homeList,getContext());
        adviserDashboardHomeVideosRv.setAdapter(adapter);

    }



    private void setRecyclerViewQuotes (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDashboardHomeQuotesRv.setLayoutManager(layoutManager);
        List <AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardHomeQuotesFragmentAdapter adapter = new AdviserDashboardHomeQuotesFragmentAdapter(homeList,getContext());
        adviserDashboardHomeQuotesRv.setAdapter(adapter);

    }

    private void clickEvent(){

        textViewAdviserHomeFragmentAllVideos .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdviserDashboardAllVideosActivity.class);
                startActivity(intent);
            }
        });

        textViewAdviserHomeFragmentAllQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdviserDashboardSeeAllQuotesActivity.class);
                startActivity(intent);
            }
        });

    }
}