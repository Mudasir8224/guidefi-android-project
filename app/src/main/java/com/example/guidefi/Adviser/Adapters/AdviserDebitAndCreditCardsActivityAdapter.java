package com.example.guidefi.Adviser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserEditDebitAndCreditCardActivity;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.List;

public class AdviserDebitAndCreditCardsActivityAdapter extends RecyclerView.Adapter<AdviserDebitAndCreditCardsActivityAdapter.viewHolderMostPopular> {

    List<AdviserHomeModel> adviserHomeModelList;
    private Context context;

    public AdviserDebitAndCreditCardsActivityAdapter(List<AdviserHomeModel> adviserHomeModelList, Context context) {
        this.adviserHomeModelList = adviserHomeModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.adviser_debitandcredit_cards_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final AdviserHomeModel adviserHomeModel = adviserHomeModelList.get(position);




    }

    @Override
    public int getItemCount() {
        return adviserHomeModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        LinearLayout layoutEditCard;

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

            layoutEditCard = itemView.findViewById(R.id.layoutEditCard);

            layoutEditCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, AdviserEditDebitAndCreditCardActivity.class);
                    context.startActivity(intent);
                }
            });


        }
    }
}
