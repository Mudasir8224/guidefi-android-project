package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class AdviserTermAndConditionActivity extends AppCompatActivity {

    Button buttonTermAndConditionNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_term_and_condition);

        buttonTermAndConditionNext = findViewById(R.id.buttonTermAndConditionNext);

        buttonTermAndConditionNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserTermAndConditionActivity.this,AdviserRegistrationMessageActivity.class);
                startActivity(intent);
            }
        });
    }
}