package com.example.guidefi.Adviser.Activities.AdviserAddRateCardActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class AdviserRateCardCheckOutActivity extends AppCompatActivity {

    Button buttonAdviserRateCardCheckOutActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_rate_card_check_out);

        init();

    }

    private void init(){
        buttonAdviserRateCardCheckOutActivity = findViewById(R.id.buttonAdviserRateCardCheckOutActivity);

    }


}