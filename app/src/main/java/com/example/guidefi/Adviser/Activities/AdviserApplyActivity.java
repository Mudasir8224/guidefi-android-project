package com.example.guidefi.Adviser.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guidefi.R;

public class AdviserApplyActivity extends AppCompatActivity {
    Button buttonAdviserApply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_apply);

        buttonAdviserApply = findViewById(R.id.buttonAdviserApply);

        buttonAdviserApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserApplyActivity.this,AdviserProfileSignUpActivity.class);
                startActivity(intent);

            }
        });
    }
}