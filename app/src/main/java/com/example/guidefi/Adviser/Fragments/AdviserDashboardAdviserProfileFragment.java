package com.example.guidefi.Adviser.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.guidefi.Adviser.Activities.AdviserProfileEditActivity;
import com.example.guidefi.Adviser.Activities.AdviserProfileShareActivity;
import com.example.guidefi.Adviser.Adapters.AdviserProfileFragmentClientsFeedbackAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardAdviserProfileFragment extends Fragment {

    RecyclerView adviserProfileFeedbackFromClientsRv;
    ImageView imageViewAdviserEditProfile;
    ImageView imageViewAdviserShareProfile;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_adviser_dashboard_adviser_profile, container, false);

        adviserProfileFeedbackFromClientsRv = rootView.findViewById(R.id.adviserProfileFeedbackFromClientsRv);
        imageViewAdviserEditProfile = rootView.findViewById(R.id.imageViewAdviserEditProfile);
        imageViewAdviserShareProfile = rootView.findViewById(R.id.imageViewAdviserShareProfile);
        setRecyclerView();
        clickEvents();
        return rootView;
    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserProfileFeedbackFromClientsRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserProfileFragmentClientsFeedbackAdapter adapter = new AdviserProfileFragmentClientsFeedbackAdapter(homeList,getContext());
        adviserProfileFeedbackFromClientsRv.setAdapter(adapter);

    }

    private void clickEvents(){
        imageViewAdviserEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdviserProfileEditActivity.class);
                startActivity(intent);
            }
        });

        imageViewAdviserShareProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdviserProfileShareActivity.class);
                startActivity(intent);
            }
        });

    }


}