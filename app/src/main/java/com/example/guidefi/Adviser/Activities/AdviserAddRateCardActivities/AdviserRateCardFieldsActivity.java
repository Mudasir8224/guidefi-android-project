package com.example.guidefi.Adviser.Activities.AdviserAddRateCardActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class AdviserRateCardFieldsActivity extends AppCompatActivity {

    Button buttonAdviserRateCardFieldsNextActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_rate_card_fields);
        init();
        clickEvents();

    }

    private void init(){
        buttonAdviserRateCardFieldsNextActivity = findViewById(R.id.buttonAdviserRateCardFieldsNextActivity);

    }

    private void clickEvents(){
        buttonAdviserRateCardFieldsNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserRateCardFieldsActivity.this,AdviserRateCardActivity.class);
                startActivity(intent);
            }
        });
    }


}