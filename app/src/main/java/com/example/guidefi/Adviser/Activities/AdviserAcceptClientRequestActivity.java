package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.Adviser.Fragments.SubModuleFragmentsAdviserClients.AdviserDashboardPotentialClientsSubFragment;
import com.example.guidefi.R;

public class AdviserAcceptClientRequestActivity extends AppCompatActivity {

    Button buttonAdviserCancelPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_accept_client_request);

        init();
        clickEvent();

    }

    private void init() {
        buttonAdviserCancelPayment = findViewById(R.id.buttonAdviserCancelPayment);
    }

    private void clickEvent() {
        buttonAdviserCancelPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }
}