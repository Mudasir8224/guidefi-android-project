package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeAllVideosActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeVideosFragmentAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardAllVideosActivity extends AppCompatActivity {

    RecyclerView adviserDashboardHomeAllVideosActivityRv;
    TextView textViewAdviserDashboardAddNewVideosActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_dashboard_all_videos);
        init();
        setRecyclerViewAllVideos();
        clickEvents();

    }

    public void init(){
        adviserDashboardHomeAllVideosActivityRv = findViewById(R.id.adviserDashboardHomeAllVideosActivityRv);
        textViewAdviserDashboardAddNewVideosActivity = findViewById(R.id.textViewAdviserDashboardAddNewVideosActivity);
    }

    private void setRecyclerViewAllVideos (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDashboardHomeAllVideosActivityRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardHomeAllVideosActivityAdapter adapter = new AdviserDashboardHomeAllVideosActivityAdapter(homeList,this);
        adviserDashboardHomeAllVideosActivityRv.setAdapter(adapter);

    }

    private void clickEvents(){
        textViewAdviserDashboardAddNewVideosActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserDashboardAllVideosActivity.this,AdviserDashboardAddNewVideosActivity.class);
                startActivity(intent);
            }
        });
    }
}