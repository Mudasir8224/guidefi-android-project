package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.guidefi.R;

public class AdviserDashboardFullScreenVideoPreviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_dashboard_full_screen_video_preview);
    }
}