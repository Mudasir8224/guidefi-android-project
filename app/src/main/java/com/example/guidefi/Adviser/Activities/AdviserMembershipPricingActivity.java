package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeSeeAllQuotesActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserMembershipPricingActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserMembershipPricingActivity extends AppCompatActivity {

    Button button;
    RecyclerView adviserMembershipPricingRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_membership_pricing);

        init();
        clickEvents();
        setRecyclerView();

    }

    private void init(){
        button = findViewById(R.id.buttonPricingSelect);
        adviserMembershipPricingRv = findViewById(R.id.adviserMembershipPricingRv);
    }

    private void clickEvents(){

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserMembershipPricingActivity.this,AdviserLogInActivity.class);
                startActivity(intent);
            }
        });

    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserMembershipPricingRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserMembershipPricingActivityAdapter adapter = new AdviserMembershipPricingActivityAdapter(homeList,this);
        adviserMembershipPricingRv.setAdapter(adapter);

    }
}