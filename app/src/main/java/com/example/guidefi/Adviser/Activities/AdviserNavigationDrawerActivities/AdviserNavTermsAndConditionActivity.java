package com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.R;

public class AdviserNavTermsAndConditionActivity extends AppCompatActivity {

    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_nav_terms_and_condition);

        init();
        clickEvents();


    }


    private void init(){
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }

    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

    }

}