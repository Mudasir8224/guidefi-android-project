package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.Adviser.Activities.AdviserDashboardActivity.AdviserDashboardActivity;
import com.example.guidefi.R;

public class AdviserGuideMatchActivity extends AppCompatActivity {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_guide_match);

        button = findViewById(R.id.buttonAdviserGuideMatchOk);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdviserGuideMatchActivity.this, AdviserDashboardActivity.class);
                startActivity(intent);
            }
        });
    }
}