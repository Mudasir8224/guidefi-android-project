package com.example.guidefi.Adviser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Activities.AdviserClientProfileViewActivity;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.List;

public class AdviserDashboardAllClientsSubFragmentAdapter extends RecyclerView.Adapter<AdviserDashboardAllClientsSubFragmentAdapter.viewHolderMostPopular> {

    List<AdviserHomeModel> adviserHomeModelList;
    private Context context;

    public AdviserDashboardAllClientsSubFragmentAdapter(List<AdviserHomeModel> adviserHomeModelList, Context context) {
        this.adviserHomeModelList = adviserHomeModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.adviser_dashboard_allclients_subfragment_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final AdviserHomeModel adviserHomeModel = adviserHomeModelList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AdviserClientProfileViewActivity.class);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return adviserHomeModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

        }
    }
}
