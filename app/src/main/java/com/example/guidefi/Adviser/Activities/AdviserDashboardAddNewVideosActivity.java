package com.example.guidefi.Adviser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeAddNewVideosActivityAdapter;
import com.example.guidefi.Adviser.Adapters.AdviserDashboardHomeAllVideosActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class AdviserDashboardAddNewVideosActivity extends AppCompatActivity {
    RecyclerView adviserDashboardHomeAddNewVideosActivityRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adviser_dashboard_add_new_videos);
        init();
        setRecyclerViewAddNewVideos();
    }


    public void init(){
        adviserDashboardHomeAddNewVideosActivityRv = findViewById(R.id.adviserDashboardHomeAddNewVideosActivityRv);
    }

    private void setRecyclerViewAddNewVideos (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adviserDashboardHomeAddNewVideosActivityRv.setLayoutManager(layoutManager);
        List<AdviserHomeModel> homeList = new ArrayList<>();
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        homeList.add(new AdviserHomeModel());
        AdviserDashboardHomeAddNewVideosActivityAdapter adapter = new AdviserDashboardHomeAddNewVideosActivityAdapter(homeList,this);
        adviserDashboardHomeAddNewVideosActivityRv.setAdapter(adapter);

    }
}