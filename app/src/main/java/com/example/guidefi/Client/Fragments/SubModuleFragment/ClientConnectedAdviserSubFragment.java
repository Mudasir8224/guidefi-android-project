package com.example.guidefi.Client.Fragments.SubModuleFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientConnectedAdviserSubFragmentAdapter;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientConnectedAdviserSubFragment extends Fragment {
  RecyclerView clientConnectedAdvisorFragmentRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_connected_adviser_sub, container, false);

        clientConnectedAdvisorFragmentRv = rootView.findViewById(R.id.clientConnectedAdvisorFragmentRv);
        setRecyclerView();
        return rootView;


    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientConnectedAdvisorFragmentRv.setLayoutManager(layoutManager);
        List<ClientHomeModel> homeList = new ArrayList<>();
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        ClientConnectedAdviserSubFragmentAdapter clientConnectedAdviserSubFragmentAdapter = new ClientConnectedAdviserSubFragmentAdapter(homeList,getContext());
        clientConnectedAdvisorFragmentRv.setAdapter(clientConnectedAdviserSubFragmentAdapter);

    }

}