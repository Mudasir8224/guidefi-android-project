package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.List;

public class ClientAdviserProfileViewActivityClientFeedbackAdapter extends RecyclerView.Adapter<ClientAdviserProfileViewActivityClientFeedbackAdapter.viewHolderMostPopular> {

    List<ClientFragmentsModel> fragmentsModelList;
    private Context context;

    public ClientAdviserProfileViewActivityClientFeedbackAdapter(List<ClientFragmentsModel> fragmentsModelList, Context context) {
        this.fragmentsModelList = fragmentsModelList ;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_adviser_profileview_activity_client_feedback_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final ClientFragmentsModel model = fragmentsModelList.get(position);


    }

    @Override
    public int getItemCount() {
        return fragmentsModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

        }
    }
}
