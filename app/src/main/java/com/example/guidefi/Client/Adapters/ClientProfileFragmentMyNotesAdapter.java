package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Client.Activities.ClientProfileFragmentNotesActivity;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.List;

public class ClientProfileFragmentMyNotesAdapter extends RecyclerView.Adapter<ClientProfileFragmentMyNotesAdapter.viewHolderMostPopular> {

    List<ClientFragmentsModel> clientFragmentsModelList;
    private Context context;

    public ClientProfileFragmentMyNotesAdapter(List<ClientFragmentsModel> clientFragmentsModelList, Context context) {
        this.clientFragmentsModelList = clientFragmentsModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_profilefragment_mynotes_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final ClientFragmentsModel clientFragmentsModel = clientFragmentsModelList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ClientProfileFragmentNotesActivity.class);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return clientFragmentsModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

        }
    }
}
