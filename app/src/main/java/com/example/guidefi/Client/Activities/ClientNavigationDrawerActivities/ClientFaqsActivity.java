package com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Adviser.Adapters.AdviserFaqsActivityAdapter;
import com.example.guidefi.Adviser.Models.AdviserFaqsModel;
import com.example.guidefi.Client.Adapters.ClientFaqsActivityAdapter;
import com.example.guidefi.Client.Models.ClientFaqsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientFaqsActivity extends AppCompatActivity {

    RecyclerView clientFaqsRv;
    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_faqs);

        init();
        setRecyclerView();
        clickEvents();


    }





    private void init(){
        clientFaqsRv = findViewById(R.id.clientFaqsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientFaqsRv.setLayoutManager(layoutManager);
        List<ClientFaqsModel> faqsModels = new ArrayList<>();

        faqsModels.add(new ClientFaqsModel("Question 1","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new ClientFaqsModel("Question 2","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new ClientFaqsModel("Question 3","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));
        faqsModels.add(new ClientFaqsModel("Question 4","A reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years," +
                " sometimes by accident, sometimes on purpose injected humour and the like"));

        ClientFaqsActivityAdapter adapter = new ClientFaqsActivityAdapter(faqsModels,this);
        clientFaqsRv.setAdapter(adapter);


    }

    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }



}