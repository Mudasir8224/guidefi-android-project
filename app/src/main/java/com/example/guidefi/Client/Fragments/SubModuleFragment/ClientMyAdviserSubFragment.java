package com.example.guidefi.Client.Fragments.SubModuleFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientMyAdvisersFragmentAdapter;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;


public class ClientMyAdviserSubFragment extends Fragment {

    RecyclerView clientMyAdvisorFragmentRv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_my_adviser_sub, container, false);

        clientMyAdvisorFragmentRv = rootView.findViewById(R.id.clientMyAdvisorFragmentRv);

        setRecyclerView();
        return rootView;

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientMyAdvisorFragmentRv.setLayoutManager(layoutManager);
        List<ClientHomeModel> homeList = new ArrayList<>();
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        ClientMyAdvisersFragmentAdapter clientMyAdvisersFragmentAdapter = new ClientMyAdvisersFragmentAdapter(homeList,getContext());
        clientMyAdvisorFragmentRv.setAdapter(clientMyAdvisersFragmentAdapter);

    }

}