package com.example.guidefi.Client.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.Client.Activities.ClientDashboardActivity.ClientDashBoardActivity;
import com.example.guidefi.R;

public class ClientLoginActivity extends AppCompatActivity {
    Button buttonSignIn;
    Button buttonLogIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_client);

        init();


        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClientLoginActivity.this, ClientProfileSignUpActivity.class);
                startActivity(intent);
            }
        });

        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClientLoginActivity.this, ClientDashBoardActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init(){
        buttonSignIn = findViewById(R.id.buttonSignIn);
        buttonLogIn = findViewById(R.id.buttonLogIn);
    }
}