package com.example.guidefi.Client.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.guidefi.Client.Adapters.ClientProfileFragmentMyNotesAdapter;
import com.example.guidefi.Client.Adapters.ClientProfileFragmentNotesActivityAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientProfileFragmentNotesActivity extends AppCompatActivity {

    RecyclerView clientProfileFragmentNotesActivityRv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile_fragment_notes);

        init();
        setRecyclerView();
    }



    private void init(){

        clientProfileFragmentNotesActivityRv = findViewById(R.id.clientProfileFragmentNotesActivityRv);

    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientProfileFragmentNotesActivityRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientProfileFragmentNotesActivityAdapter adapter = new ClientProfileFragmentNotesActivityAdapter(modelList,this);
        clientProfileFragmentNotesActivityRv.setAdapter(adapter);

    }


}