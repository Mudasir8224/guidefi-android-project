package com.example.guidefi.Client.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientConnectedAdviserSubFragment;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientAdviserSubFragment;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientMyAdviserSubFragment;
import com.example.guidefi.Common.Helpers.ViewPagerFragmentManager;
import com.example.guidefi.Common.Views.ViewPagerV2;
import com.example.guidefi.R;

public class ClientAdviserFragment extends Fragment {

    Button buttonMyAdviser;
    Button buttonConnectedAdviser;
    Button buttonOffers;
    ViewPagerV2 myViewPagerClientAdviserFragment;
    ViewPagerFragmentManager viewPagerFragmentManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_adviser, container, false);

        buttonMyAdviser = rootView.findViewById(R.id.buttonMyAdviser);
        buttonConnectedAdviser = rootView.findViewById(R.id.buttonConnectedAdviser);
        buttonOffers = rootView.findViewById(R.id.buttonOffers);
        myViewPagerClientAdviserFragment = rootView.findViewById(R.id.myViewPagerClientAdviserFragment);

        setupFragment();
        clickEvent();
        businessLogic();

        return rootView;
    }

    private void setupFragment(){
        this.viewPagerFragmentManager = new ViewPagerFragmentManager(getChildFragmentManager());
        this.viewPagerFragmentManager.addFragments(new Fragment());
        this.viewPagerFragmentManager.addFragments(new ClientAdviserSubFragment());
        this.viewPagerFragmentManager.addFragments(new ClientConnectedAdviserSubFragment());
        this.viewPagerFragmentManager.addFragments(new ClientMyAdviserSubFragment());
        myViewPagerClientAdviserFragment.setAdapter(viewPagerFragmentManager);
        myViewPagerClientAdviserFragment.disablescroll(true);

    }


    private void clickEvent() {

        buttonMyAdviser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myViewPagerClientAdviserFragment.setCurrentItem(1, true);

                buttonMyAdviser.setTextColor(Color.BLACK);
                buttonConnectedAdviser.setTextColor(Color.parseColor("#736469"));
                buttonOffers.setTextColor(Color.parseColor("#736469"));


            }
        });


        buttonConnectedAdviser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myViewPagerClientAdviserFragment.setCurrentItem(2, true);

                buttonConnectedAdviser.setTextColor(Color.BLACK);
                buttonMyAdviser.setTextColor(Color.parseColor("#736469"));
                buttonOffers.setTextColor(Color.parseColor("#736469"));

            }
        });

        buttonOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myViewPagerClientAdviserFragment.setCurrentItem(3, true);

                buttonOffers.setTextColor(Color.BLACK);
                buttonConnectedAdviser.setTextColor(Color.parseColor("#736469"));
                buttonMyAdviser.setTextColor(Color.parseColor("#736469"));

            }
        });
    }

    private void businessLogic() {

        buttonMyAdviser.setTextColor(Color.BLACK);
        buttonConnectedAdviser.setTextColor(Color.parseColor("#736469"));
        buttonOffers.setTextColor(Color.parseColor("#736469"));

    }

}