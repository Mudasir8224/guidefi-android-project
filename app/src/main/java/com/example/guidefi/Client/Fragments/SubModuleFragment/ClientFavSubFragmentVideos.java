package com.example.guidefi.Client.Fragments.SubModuleFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientFavSubFragmentVideosAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientFavSubFragmentVideos extends Fragment {

    RecyclerView clientFavSubFragmentVideosRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_fav_sub_videos, container, false);

        clientFavSubFragmentVideosRv = rootView.findViewById(R.id.clientFavSubFragmentVideosRv);
        setRecyclerView();
        return rootView;
    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientFavSubFragmentVideosRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientFavSubFragmentVideosAdapter adapter = new ClientFavSubFragmentVideosAdapter(modelList,getContext());
        clientFavSubFragmentVideosRv.setAdapter(adapter);

    }


}