package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Activities.ClientAdviserProfileViewActivity;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.example.guidefi.Client.Models.ClientHomeModel.FIRST_LAYOUT;

public class ClientMyAdvisersFragmentAdapter extends RecyclerView.Adapter {

    private List<ClientHomeModel> homeList;
    private Context context;

    public ClientMyAdvisersFragmentAdapter(List<ClientHomeModel> homeList, Context context) {
        this.homeList = homeList;
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {
        //  return super.getItemViewType(position);
        switch (homeList.get(position).getViewType()) {

            case 0:
                return FIRST_LAYOUT;

            default:
                return -1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case FIRST_LAYOUT:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.client_myadviser_subfragment_advisor_view, parent, false);
                return new firstLayout(view);
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ClientAdviserProfileViewActivity.class);
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return this.homeList.size();
    }

    class firstLayout extends RecyclerView.ViewHolder {

        public firstLayout(@NonNull View itemView) {
            super(itemView);
        }
    }


}
