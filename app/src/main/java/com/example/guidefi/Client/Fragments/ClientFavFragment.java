package com.example.guidefi.Client.Fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientFavSubFragmentQuotes;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientFavSubFragmentVideos;
import com.example.guidefi.Client.Fragments.SubModuleFragment.ClientFavSubFragmentWatchLaterVideo;
import com.example.guidefi.Client.Helpers.TabAdapter;
import com.example.guidefi.Common.Views.ViewPagerV2;
import com.example.guidefi.R;
import com.google.android.material.tabs.TabLayout;

public class ClientFavFragment extends Fragment {

    TabLayout tabLayoutFavFragment;
    ViewPagerV2 myViewPagerFavFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_fav, container, false);

        tabLayoutFavFragment = rootView.findViewById(R.id.tabLayoutFavFragment);
        myViewPagerFavFragment = rootView.findViewById(R.id.myViewPagerFavFragment);
        setupFragment();
        return rootView;
    }

    private void setupFragment(){
        TabAdapter tabAdapter = new TabAdapter(getChildFragmentManager());
        tabAdapter.addFragment(new ClientFavSubFragmentVideos(),"Videos");
        tabAdapter.addFragment(new ClientFavSubFragmentWatchLaterVideo(),"Watch Later");
        tabAdapter.addFragment(new ClientFavSubFragmentQuotes(),"Quotes");
        myViewPagerFavFragment.setAdapter(tabAdapter);
        tabLayoutFavFragment.setupWithViewPager(myViewPagerFavFragment);

    }


}