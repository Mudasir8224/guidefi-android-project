package com.example.guidefi.Client.Fragments;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientFavSubFragmentVideosAdapter;
import com.example.guidefi.Client.Adapters.ClientProfileFragmentMyNotesAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientProfileFragment extends Fragment {

    RecyclerView clientProfileFragmentMyNotesRv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_profile, container, false);

        clientProfileFragmentMyNotesRv = rootView.findViewById(R.id.clientProfileFragmentMyNotesRv);

        setRecyclerView();

        return rootView;
    }

    private void setRecyclerView (){

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        clientProfileFragmentMyNotesRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientProfileFragmentMyNotesAdapter adapter = new ClientProfileFragmentMyNotesAdapter(modelList,getContext());
        clientProfileFragmentMyNotesRv.setAdapter(adapter);


    }


}