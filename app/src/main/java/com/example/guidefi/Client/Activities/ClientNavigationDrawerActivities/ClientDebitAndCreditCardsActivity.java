package com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Client.Adapters.ClientDebitAndCreditCardsActivityAdapter;
import com.example.guidefi.Client.Adapters.ClientPaymentsActivityAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientDebitAndCreditCardsActivity extends AppCompatActivity {

    RecyclerView clientDebitAndCreditCardsRv;
    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_debit_and_credit_cards);

        init();
        setRecyclerView();
        clickEvents();


    }

    private void init(){
        clientDebitAndCreditCardsRv = findViewById(R.id.clientDebitAndCreditCardsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientDebitAndCreditCardsRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientDebitAndCreditCardsActivityAdapter adapter = new ClientDebitAndCreditCardsActivityAdapter(modelList,this);
        clientDebitAndCreditCardsRv.setAdapter(adapter);

    }

    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


}