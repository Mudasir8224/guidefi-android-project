package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Models.AdviserFaqsModel;
import com.example.guidefi.Client.Models.ClientFaqsModel;
import com.example.guidefi.R;

import java.util.List;

public class ClientFaqsActivityAdapter extends RecyclerView.Adapter<ClientFaqsActivityAdapter.viewHolderMostPopular> {

    List<ClientFaqsModel> faqsModelList;
    private Context context;

    public ClientFaqsActivityAdapter(List<ClientFaqsModel> faqsModelList, Context context) {
        this.faqsModelList = faqsModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_navigation_faqs_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
         final ClientFaqsModel faqsModel = faqsModelList.get(position);

        holder.bindFaqs(faqsModel);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean expanded = faqsModel.isExpanded();
                faqsModel.setExpanded(!expanded);
                notifyItemChanged(position);

            }
        });


    }

    @Override
    public int getItemCount() {

        return faqsModelList == null ? 0 : faqsModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        LinearLayout linearLayoutDropDown;
        TextView textViewQuestions;
        TextView textViewQuestionDetails;
        ImageView imageViewQuestions;


        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

            linearLayoutDropDown = itemView.findViewById(R.id.linearLayoutDropDown);

            textViewQuestions = itemView.findViewById(R.id.textViewQuestions);
            textViewQuestionDetails = itemView.findViewById(R.id.textViewQuestionDetails);
            imageViewQuestions = itemView.findViewById(R.id.imageViewQuestions);

            linearLayoutDropDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageViewQuestions.setBackgroundResource(R.drawable.ic_baseline_arrow_drop_down_24);


                }
            });


        }


        private void bindFaqs(ClientFaqsModel faqsModel){

            boolean expanded =  faqsModel.isExpanded();

            linearLayoutDropDown.setVisibility(expanded ? View.VISIBLE : View.GONE);

            textViewQuestions.setText(faqsModel.getTitle());
            textViewQuestionDetails.setText(faqsModel.getDetail());


        }


    }


}
