package com.example.guidefi.Client.Models;

public class ClientFaqsModel {

    private String title;
    private String detail;

    private boolean expanded;

    public ClientFaqsModel(String title, String detail) {
        this.title = title;
        this.detail = detail;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
