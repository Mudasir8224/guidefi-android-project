package com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.guidefi.Client.Adapters.ClientDashboardHomeAdapter;
import com.example.guidefi.Client.Adapters.ClientPaymentsActivityAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientPaymentsActivity extends AppCompatActivity {

    RecyclerView clientPaymentsRv;
    ImageView imageViewLeftArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_payments);

        init();
        setRecyclerView();
        clickEvents();


    }

    private void init(){
        clientPaymentsRv = findViewById(R.id.clientPaymentsRv);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientPaymentsRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientPaymentsActivityAdapter adapter = new ClientPaymentsActivityAdapter(modelList,this);
        clientPaymentsRv.setAdapter(adapter);

    }

    private void clickEvents(){

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}