package com.example.guidefi.Client.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class ClientProfileSignUpActivity extends AppCompatActivity {
    Button buttonLogIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_profile_client);

        init();

        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClientProfileSignUpActivity.this, ClientLoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init(){
        buttonLogIn = findViewById(R.id.buttonLogIn);
    }
}