package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.List;

public class ClientFavSubFragmentWatchLaterVideosAdapter extends RecyclerView.Adapter<ClientFavSubFragmentWatchLaterVideosAdapter.viewHolderMostPopular> {

    List<ClientFragmentsModel> clientFragmentsModelList;
    private Context context;

    public ClientFavSubFragmentWatchLaterVideosAdapter(List<ClientFragmentsModel> clientFragmentsModelList, Context context) {
        this.clientFragmentsModelList = clientFragmentsModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_fav_subfragment_watchlatervideo_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final ClientFragmentsModel clientFragmentsModel = clientFragmentsModelList.get(position);


    }

    @Override
    public int getItemCount() {
        return clientFragmentsModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

        }
    }
}
