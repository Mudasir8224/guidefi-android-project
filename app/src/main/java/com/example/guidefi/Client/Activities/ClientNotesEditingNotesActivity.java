package com.example.guidefi.Client.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.guidefi.R;

public class ClientNotesEditingNotesActivity extends AppCompatActivity {

    Button buttonClientEditNotesCancel;
    Button buttonClientEditNotesSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_notes_editing_notes);

        init();
        clickEvents();
    }

    private void init(){
        buttonClientEditNotesCancel = findViewById(R.id.buttonClientEditNotesCancel);

        buttonClientEditNotesSave = findViewById(R.id.buttonClientEditNotesSave);
    }


    private void clickEvents(){
        buttonClientEditNotesCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        buttonClientEditNotesSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClientNotesEditingNotesActivity.this,ClientProfileFragmentNotesActivity.class);
                startActivity(intent);
            }
        });

    }

}