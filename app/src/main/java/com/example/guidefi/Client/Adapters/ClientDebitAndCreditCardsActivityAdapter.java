package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserEditDebitAndCreditCardActivity;
import com.example.guidefi.Adviser.Models.AdviserHomeModel;
import com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities.ClientEditDebitAndCreditCardsActivity;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.List;

public class ClientDebitAndCreditCardsActivityAdapter extends RecyclerView.Adapter<ClientDebitAndCreditCardsActivityAdapter.viewHolderMostPopular> {

    List<ClientFragmentsModel> modelList;
    private Context context;

    public ClientDebitAndCreditCardsActivityAdapter(List<ClientFragmentsModel> modelList, Context context) {
        this.modelList = modelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_debitandcredit_cards_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final ClientFragmentsModel model = modelList.get(position);




    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        LinearLayout layoutEditCard;

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

            layoutEditCard = itemView.findViewById(R.id.layoutEditCard);

            layoutEditCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, ClientEditDebitAndCreditCardsActivity.class);
                    context.startActivity(intent);
                }
            });


        }
    }
}
