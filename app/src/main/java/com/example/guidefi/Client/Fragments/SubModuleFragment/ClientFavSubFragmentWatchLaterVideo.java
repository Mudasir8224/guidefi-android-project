package com.example.guidefi.Client.Fragments.SubModuleFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientFavSubFragmentWatchLaterVideosAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientFavSubFragmentWatchLaterVideo extends Fragment {

    RecyclerView clientFavSubFragmentWatchLaterVideosRv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_fav_sub_watch_later_video, container, false);
        clientFavSubFragmentWatchLaterVideosRv = rootView.findViewById(R.id.clientFavSubFragmentWatchLaterVideosRv);
        setRecyclerView();
        return rootView;
    }


    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientFavSubFragmentWatchLaterVideosRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientFavSubFragmentWatchLaterVideosAdapter adapter = new ClientFavSubFragmentWatchLaterVideosAdapter(modelList,getContext());
        clientFavSubFragmentWatchLaterVideosRv.setAdapter(adapter);

    }

}