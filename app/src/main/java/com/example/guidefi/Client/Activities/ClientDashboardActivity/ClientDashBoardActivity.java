package com.example.guidefi.Client.Activities.ClientDashboardActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.guidefi.Adviser.Activities.AdviserDashboardActivity.AdviserDashboardActivity;
import com.example.guidefi.Adviser.Activities.AdviserNavigationDrawerActivities.AdviserPaymentsActivity;
import com.example.guidefi.Client.Activities.ClientAdviserProfileViewActivity;
import com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities.ClientDebitAndCreditCardsActivity;
import com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities.ClientFaqsActivity;
import com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities.ClientPaymentsActivity;
import com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities.ClientRebateSystemActivity;
import com.example.guidefi.Client.Fragments.ClientAdviserFragment;
import com.example.guidefi.Client.Fragments.ClientFavFragment;
import com.example.guidefi.Client.Fragments.ClientHomeFragment;
import com.example.guidefi.Client.Fragments.ClientProfileFragment;
import com.example.guidefi.Common.Helpers.ViewPagerFragmentManager;

import com.example.guidefi.Common.Views.ViewPagerV2;
import com.example.guidefi.R;
import com.google.android.material.navigation.NavigationView;

public class ClientDashBoardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ViewPagerV2 mViewPager;
    TextView textViewHome,textViewAdviser,textViewFav,textViewProfile;
    ViewPagerFragmentManager viewPagerFragmentManager;


    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_dash_client);

        init();
        setupFragment();
        clickEvent();
        businessLogic();
        setupDrawer();

    }
    private void init(){
        mViewPager = findViewById(R.id.myViewPager);
        textViewHome = findViewById(R.id.textViewHomeFragment);
        textViewAdviser = findViewById(R.id.textViewAdvisorFragment);
        textViewFav = findViewById(R.id.textViewFavFragment);
        textViewProfile = findViewById(R.id.textViewProfileFragment);


        drawerLayout = findViewById(R.id.mDrawer);
        navigationView = findViewById(R.id.nav_view);

        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

    }

    private void setupFragment(){
        this.viewPagerFragmentManager = new ViewPagerFragmentManager(getSupportFragmentManager());
        this.viewPagerFragmentManager.addFragments(new ClientHomeFragment());
        this.viewPagerFragmentManager.addFragments(new ClientAdviserFragment());
        this.viewPagerFragmentManager.addFragments(new ClientFavFragment());
        this.viewPagerFragmentManager.addFragments(new ClientProfileFragment( ));
        mViewPager.setAdapter(viewPagerFragmentManager);
        mViewPager.disablescroll(true);

    }

    private void clickEvent(){

        textViewHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0,true);

                textViewHome.setTextColor(Color.WHITE);
                textViewAdviser.setTextColor(Color.parseColor("#DFDADC"));
                textViewFav.setTextColor(Color.parseColor("#DFDADC"));
                textViewProfile.setTextColor(Color.parseColor("#DFDADC"));

            }
        });


        textViewAdviser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1,true);

                textViewAdviser.setTextColor(Color.WHITE);
                textViewHome.setTextColor(Color.parseColor("#DFDADC"));
                textViewFav.setTextColor(Color.parseColor("#DFDADC"));
                textViewProfile.setTextColor(Color.parseColor("#DFDADC"));

            }
        });


        textViewFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(2,true);

                textViewFav.setTextColor(Color.WHITE);
                textViewAdviser.setTextColor(Color.parseColor("#DFDADC"));
                textViewHome.setTextColor(Color.parseColor("#DFDADC"));
                textViewProfile.setTextColor(Color.parseColor("#DFDADC"));


            }
        });


        textViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(3,true);

                textViewProfile.setTextColor(Color.WHITE);
                textViewAdviser.setTextColor(Color.parseColor("#DFDADC"));
                textViewFav.setTextColor(Color.parseColor("#DFDADC"));
                textViewHome.setTextColor(Color.parseColor("#DFDADC"));

            }
        });

    }


    private void businessLogic(){

        textViewHome.setTextColor(Color.WHITE);
        textViewAdviser.setTextColor(Color.parseColor("#DFDADC"));
        textViewFav.setTextColor(Color.parseColor("#DFDADC"));
        textViewProfile.setTextColor(Color.parseColor("#DFDADC"));

    }


    public void setupDrawer () {

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,
                toolbar,
                R.string.open,
                R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.clientLeftNavigationPayments:
                Intent intentPayment = new Intent(ClientDashBoardActivity.this, ClientPaymentsActivity.class);
                startActivity(intentPayment);
                break;


            case R.id.clientLeftNavigationDebitAndCreditCards:
                Intent intentDebitAndCreditCards = new Intent(ClientDashBoardActivity.this, ClientDebitAndCreditCardsActivity.class);
                startActivity(intentDebitAndCreditCards);
                break;


            case R.id.clientLeftNavigationRebateSystem:
                Intent intentRebateSystem = new Intent(ClientDashBoardActivity.this, ClientRebateSystemActivity.class);
                startActivity(intentRebateSystem);
                break;



            case R.id.clientLeftNavigationFaqs:
                Intent intentFaqs = new Intent(ClientDashBoardActivity.this, ClientFaqsActivity.class);
                startActivity(intentFaqs);
                break;



        }

        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
