package com.example.guidefi.Client.Models;

public class ClientHomeModel {

    public static final int FIRST_LAYOUT = 0;
    public static final int SECOND_LAYOUT  = 1;
    public static final int THIRD_LAYOUT  = 2;

    public ClientHomeModel(int viewType) {
        this.viewType = viewType;
    }

    private int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }


}
