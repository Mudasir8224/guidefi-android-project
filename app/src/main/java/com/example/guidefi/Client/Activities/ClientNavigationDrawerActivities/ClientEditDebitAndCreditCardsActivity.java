package com.example.guidefi.Client.Activities.ClientNavigationDrawerActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientEditDebitAndCreditCardsActivity extends AppCompatActivity {

    LinearLayout layoutSave;
    ImageView imageViewLeftArrow;

    Spinner spinnerMonths;
    Spinner spinnerYears;
    List<String> spinnerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_edit_debit_and_credit_cards);

        init();
        setUpMonthsSpinner();
        setUpYearsSpinner();
        clickEvents();


    }


    private void init(){
        layoutSave = findViewById(R.id.layoutSave);
        imageViewLeftArrow = findViewById(R.id.imageViewLeftArrow);

        spinnerMonths = findViewById(R.id.spinnerMonths);
        spinnerYears = findViewById(R.id.spinnerYears);

    }

    private void initList(String val1,String val2,String val3,String val4,String val5,String val6,String val7,
                          String val8,String val9,String val10,String val11,String val12){

        spinnerList = new ArrayList<>();
        spinnerList.add(val1);
        spinnerList.add(val2);
        spinnerList.add(val3);
        spinnerList.add(val4);
        spinnerList.add(val5);
        spinnerList.add(val6);
        spinnerList.add(val7);
        spinnerList.add(val8);
        spinnerList.add(val9);
        spinnerList.add(val10);
        spinnerList.add(val11);
        spinnerList.add(val12);

    }

    private void setUpMonthsSpinner() {

        initList("January","February","March","April","May","June","July","August","September",
                "October","November","December");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonths.setAdapter(arrayAdapter);

        spinnerMonths.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private void setUpYearsSpinner() {

        initList("2020","2021","2022","2023","2024","2023","2024","2025","2026"
                ,"2027","2028","2029");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYears.setAdapter(arrayAdapter);

        spinnerYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private void clickEvents(){
        layoutSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageViewLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}