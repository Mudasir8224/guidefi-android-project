package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ClientConnectedAdviserSubFragmentAdapter extends RecyclerView.Adapter<ClientConnectedAdviserSubFragmentAdapter.viewHolderMostPopular> {

    List<ClientHomeModel> clientHomeModelList;
    private Context context;

    public ClientConnectedAdviserSubFragmentAdapter(List<ClientHomeModel> clientHomeModelList, Context context) {
        this.clientHomeModelList = clientHomeModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolderMostPopular onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate( R.layout.client_connectedadviser_subfragment_view,parent,false);
        return new viewHolderMostPopular(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderMostPopular holder, final int position) {
        final ClientHomeModel clientHomeModel = clientHomeModelList.get(position);


    }

    @Override
    public int getItemCount() {
        return clientHomeModelList.size();
    }

    public class viewHolderMostPopular extends RecyclerView.ViewHolder{

        public viewHolderMostPopular(@NonNull View itemView) {
            super( itemView );

        }
    }
}
