package com.example.guidefi.Client.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientDashboardHomeAdapter;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientHomeFragment extends Fragment {

    RecyclerView clientHomeRv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home_client, container, false);
        clientHomeRv = rootView.findViewById(R.id.clientHomeRv);
        setRecyclerView();
        return rootView;
    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientHomeRv.setLayoutManager(layoutManager);
        List <ClientHomeModel> homeList = new ArrayList<>();
        homeList.add(new ClientHomeModel(ClientHomeModel.FIRST_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.SECOND_LAYOUT));
        homeList.add(new ClientHomeModel(ClientHomeModel.THIRD_LAYOUT));
        ClientDashboardHomeAdapter clientDashboardHomeAdapter = new ClientDashboardHomeAdapter(homeList,getContext());
        clientHomeRv.setAdapter(clientDashboardHomeAdapter);

    }

}