package com.example.guidefi.Client.Fragments.SubModuleFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Adapters.ClientFavSubFragmentQuotesAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientFavSubFragmentQuotes extends Fragment {

    RecyclerView clientFavSubFragmentQuotesRv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_client_fav_sub_quotes, container, false);

        clientFavSubFragmentQuotesRv = rootView.findViewById(R.id.clientFavSubFragmentQuotesRv);
        setRecyclerView();
        return rootView;
    }

    private void setRecyclerView (){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientFavSubFragmentQuotesRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientFavSubFragmentQuotesAdapter adapter = new ClientFavSubFragmentQuotesAdapter(modelList,getContext());
        clientFavSubFragmentQuotesRv.setAdapter(adapter);

    }

}