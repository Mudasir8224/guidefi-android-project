package com.example.guidefi.Client.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.guidefi.Client.Adapters.ClientAdviserProfileViewActivityAdviserFeedbackAdapter;
import com.example.guidefi.Client.Adapters.ClientAdviserProfileViewActivityClientFeedbackAdapter;
import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.ArrayList;
import java.util.List;

public class ClientAdviserProfileViewActivity extends AppCompatActivity {

    RecyclerView clientAdviserProfileViewClientFeedbackRv;
    RecyclerView clientAdviserProfileViewAdviserFeedbackRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_adviser_profile_view);
        init();
        setRecyclerViewClientFeedback();
        setRecyclerViewAdviserFeedback();


    }


    public void setRecyclerViewClientFeedback (){

       LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientAdviserProfileViewClientFeedbackRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientAdviserProfileViewActivityClientFeedbackAdapter adapter = new ClientAdviserProfileViewActivityClientFeedbackAdapter(modelList,this);
        clientAdviserProfileViewClientFeedbackRv.setAdapter(adapter);

    }



    public void setRecyclerViewAdviserFeedback (){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientAdviserProfileViewAdviserFeedbackRv.setLayoutManager(layoutManager);
        List<ClientFragmentsModel> modelList = new ArrayList<>();
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        modelList.add(new ClientFragmentsModel());
        ClientAdviserProfileViewActivityAdviserFeedbackAdapter adapter = new ClientAdviserProfileViewActivityAdviserFeedbackAdapter(modelList,this);
        clientAdviserProfileViewAdviserFeedbackRv.setAdapter(adapter);

    }


    public void init (){

        clientAdviserProfileViewClientFeedbackRv = findViewById(R.id.clientAdviserProfileViewClientFeedbackRv);
        clientAdviserProfileViewAdviserFeedbackRv = findViewById(R.id.clientAdviserProfileViewAdviserFeedbackRv);
    }

}