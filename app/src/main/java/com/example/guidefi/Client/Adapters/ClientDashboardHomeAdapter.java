package com.example.guidefi.Client.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guidefi.Client.Models.ClientFragmentsModel;
import com.example.guidefi.Client.Models.ClientHomeModel;
import com.example.guidefi.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.example.guidefi.Client.Models.ClientHomeModel.FIRST_LAYOUT;
import static com.example.guidefi.Client.Models.ClientHomeModel.SECOND_LAYOUT;
import static com.example.guidefi.Client.Models.ClientHomeModel.THIRD_LAYOUT;

public class ClientDashboardHomeAdapter extends RecyclerView.Adapter {

    private List<ClientHomeModel> homeList;
    private Context context;

    public ClientDashboardHomeAdapter(List<ClientHomeModel> homeList, Context context) {
        this.homeList = homeList;
        this.context = context;
    }



    @Override
    public int getItemViewType(int position) {
        //  return super.getItemViewType(position);
        switch (homeList.get(position).getViewType()) {

            case 0:
                return FIRST_LAYOUT;
            case 1:
                return SECOND_LAYOUT;
            case 2:
                return THIRD_LAYOUT;
            default:
                return -1;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case FIRST_LAYOUT:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.client_dashboard_video_view, parent, false);
                return new firstLayout(view);
            case SECOND_LAYOUT:
               LayoutInflater inflater2 = LayoutInflater.from(parent.getContext());
                View view2 = inflater2.inflate(R.layout.client_dashboard_videotwo_view, parent, false);
                return new secondLayout(view2);
            case THIRD_LAYOUT:
             LayoutInflater inflater3 = LayoutInflater.from(parent.getContext());
                View view3 = inflater3.inflate(R.layout.client_dashboard_quotes_view, parent, false);
                return new thirdLayout(view3);
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


    }


    @Override
    public int getItemCount() {
        return this.homeList.size();
    }

    class firstLayout extends RecyclerView.ViewHolder {

        public firstLayout(@NonNull View itemView) {
            super(itemView);
        }
    }

    class secondLayout extends RecyclerView.ViewHolder {

        public secondLayout(@NonNull View itemView) {
            super(itemView);
        }
    }

    class thirdLayout extends RecyclerView.ViewHolder {


        public thirdLayout(@NonNull View itemView) {
            super(itemView);
        }
    }


}
